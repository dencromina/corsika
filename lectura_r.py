import numpy as np
import matplotlib.pyplot as plt
from math import pi
import pandas as pd
from itertools import compress
from astropy.io import ascii

# =========  DATA CARD =========
with open('all', 'r') as f:
    read_data = f.read()
    #print(read_data) 
    datacard=ascii.read(read_data,format='fixed_width_no_header', delimiter=' '
                        ,col_starts=(0, 8, 39))
f.closed

nshower=int(datacard[2][1].split()[0])
#print('Num_showers:',nshower)
E_prim=float(datacard[5][1].split()[1])
#print('E_primary (GeV):',E_prim)
prim_part=int(datacard[3][1].split()[0])
if prim_part ==1:
    #print('ID_prim_particle: gamma')
    prim_part='gamma'
if prim_part ==14:
    prim_part='proton'
seed1=int(datacard[8][1].split()[0])
seed2=int(datacard[9][1].split()[0])
#print('Seeds:',seed1,',',seed2)
theta_p=float(datacard[6][1].split()[0])
print('Theta prim. part. incidence:',theta_p, 'deg')
phi_p=float(datacard[7][1].split()[0])
#print('Phi prim. part. incidence:',phi_p, 'deg')
obs_level=float(datacard[10][1].split()[0])
obs_level=obs_level*1e-2 #in meters
#print('Obs level (masl):',obs_level)
atm_mod=int(datacard[18][1].split()[0])
#print('Atmosp model:',atm_mod)
cersize=float(datacard[23][1].split()[0])
#print('Cerenk_bunch_size:',cersize)
fluorsize=float(datacard[24][1].split()[0])
#print('Fluor_bunch_size:',fluorsize)
x_area=float(datacard[27][1].split()[4])
y_area=float(datacard[27][1].split()[5])
#print('Detector size:',x_area)
#print('---------------------')

col_names = ['Nphot','x','y','t','w','uemis','vemis','wemis','arrtime']
datc = pd.read_csv('photons.dat',chunksize=100000,names=col_names)

fov=np.cos(5*pi/180)
# Histograms definition
binsize = 10 #meters
maxlen  = 1e-2 * x_area / 2
numbins = int(maxlen / binsize)
breaks=numbins+1
bunches = np.array([]).reshape(0,7)
#with_fov='t'

if x_area != y_area:
    distances=np.linspace(0,maxlen,breaks) # x
    mids=((distances[1]-distances[0])/2)+distances
    mids=mids[0:numbins]
    type_of_hist='x'
    print("Histogramming along x-axis...")
    hist=np.zeros(numbins)
else:
    radius=np.linspace(0,maxlen,breaks) # r
    mids=((radius[1]-radius[0])/2)+radius
    mids=mids[0:numbins]
    ring=pi*((radius[1:])**2-(radius[0:numbins])**2)
    type_of_hist='r'
    print("Histogramming radially...")
    hist=np.zeros(numbins)
    hist_fov=np.zeros(numbins)


# Histograma lineal CHERENKOV =====================================================================
for iterator in datc:
#   No constraints
    radio=np.sqrt(iterator.x**2+iterator.y**2)
    bunch=iterator.Nphot
    wemis=iterator.wemis
    uemis=iterator.uemis
    wemis=wemis*np.cos(-theta_p*pi/180)+uemis*np.sin(-theta_p*pi/180) # uemis=bunches[:,3]
    arrtime=iterator.arrtime
    bunch=bunch[radio<maxlen]
    arrtime=arrtime[radio<maxlen]
    radio=radio[radio<maxlen]
    ring2=ring[(radio/(radius[1]-radius[0])).astype(int) ]
    w=np.abs(bunch)/(ring2*nshower)
    h,edg=np.histogram(radio
                       ,bins=radius
                       ,weights=w
                      )
    #FoV constraint
    bunch_fov=bunch[wemis>=fov]
    radio_fov=radio[wemis>=fov]
    ring2_fov=ring[(radio_fov/(radius[1]-radius[0])).astype(int) ]
    w_fov=np.abs(bunch_fov)/(ring2_fov*nshower)
    hfov,edgfov=np.histogram(radio_fov
                       ,bins=radius
                       ,weights=w_fov
                      )

#     Store data in histograms:
    hist=hist+h
    hist_fov=hist_fov+hfov

np.savetxt('%ish_%iGeV_%ideg_offaxis_hist_r_old.dat'%(nshower,E_prim,theta_p)
      ,np.transpose([mids,hist,hist_fov])
      ,newline='\n'
      ,fmt="%7.2f %1.6e %1.6e"
      ,header=(' Num_showers:%i \n E_primary (GeV): %i \n ID_prim_particle: %s \n Seeds: %i, %i \n'
               %(nshower,E_prim,prim_part,seed1,seed2) +
               ' Theta prim. part. incidence: %i deg \n Obs level (m): %i \n Atmosp model: %i'
               %(theta_p,obs_level ,atm_mod) +
               '\n Cerenk_bunch_size: %i \n Fluor_bunch_size: %i'
               %(cersize,fluorsize) +
               '\n  \n Distance to shower axis (m) | Phot_density(1/m2)'
               )
              )
print('Histogram stored into: %ish_%iGeV_%ideg_offaxis_hist_r_old.dat'%(nshower,E_prim,theta_p))      