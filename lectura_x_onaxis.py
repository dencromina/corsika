import numpy as np
import matplotlib.pyplot as plt
from math import pi
import pandas as pd
from itertools import compress
from astropy.io import ascii

# =========  DATA CARD =========
with open('all', 'r') as f:
    read_data = f.read()
    #print(read_data) 
    datacard=ascii.read(read_data,format='fixed_width_no_header', delimiter=' '
                        ,col_starts=(0, 8, 39))
f.closed

nshower=int(datacard[2][1].split()[0])
#print('Num_showers:',nshower)
E_prim=float(datacard[5][1].split()[1])
#print('E_primary (GeV):',E_prim)
prim_part=int(datacard[3][1].split()[0])
if prim_part ==1:
    #print('ID_prim_particle: gamma')
    prim_part='gamma'
if prim_part ==14:
    prim_part='proton'
seed1=int(datacard[8][1].split()[0])
seed2=int(datacard[9][1].split()[0])
#print('Seeds:',seed1,',',seed2)
theta_p=float(datacard[6][1].split()[0])
print('Theta prim. part. incidence:',theta_p, 'deg')
phi_p=float(datacard[7][1].split()[0])
#print('Phi prim. part. incidence:',phi_p, 'deg')
obs_level=float(datacard[10][1].split()[0])
obs_level=obs_level*1e-2 #in meters
#print('Obs level (masl):',obs_level)
atm_mod=int(datacard[18][1].split()[0])
#print('Atmosp model:',atm_mod)
cersize=float(datacard[23][1].split()[0])
#print('Cerenk_bunch_size:',cersize)
fluorsize=float(datacard[24][1].split()[0])
#print('Fluor_bunch_size:',fluorsize)
x_area=float(datacard[27][1].split()[4])
y_area=float(datacard[27][1].split()[5])
#print('Detector size:',x_area)
#print('---------------------')



col_names = ['Nphot','x','y','t','w','uemis','vemis','wemis','arrtime']
#datc = pd.read_csv('cerenk.dat',chunksize=500000,names=col_names)
#datf = pd.read_csv('fluor.dat',chunksize=500000,names=col_names)

datc = pd.read_csv('photons.dat',chunksize=500000,names=col_names)

fov=np.cos(5*pi/180) #fov contraint from upward direction (+/- 5 deg)
#Cascadas inclinadas
bineado=1000
dmin=0.
dmax=10000.
cortes=bineado + 1
d=np.linspace(dmin,dmax,cortes)

mids_inc=((d[1]-d[0])/2)+d
mids=mids_inc[0:bineado]
hist=np.zeros(bineado)
   # Histograma lineal CHERENKOV

    #Cascadas inclinadas (idea coger secciones rectangulares a lo largo de una direccion
    #ie quedarme solo con un rango pequeño de x y después histogramar la variable "y")
    #se crea una mascara que seleccione solo una pequeña sección en x
angle=theta_p
binsize = 10 #meters
maxlen  = 1e-2 * x_area / 2
numbins = int(maxlen / binsize)
breaks=numbins+1
lines=0

for iterator in datc:
    x=iterator.x
    y=iterator.y
    lines=lines+len(y)
    wemis=iterator.wemis #cos direct. from z direction
    uemis=iterator.uemis #cos direct. from x direction
    wemis=wemis*np.cos(-angle*pi/180)+uemis*np.sin(-angle*pi/180)
    bunch=iterator.Nphot
    bunch=bunch[wemis>=fov]
    x=x[wemis>=fov]
    bunch=bunch[x>=0]
    x=x[x>=0]
    h,d=np.histogram(x
                       ,bins=d
                       ,weights=np.abs(bunch)/(binsize**2*nshower) #bin area
                      )
    hist=hist+h

np.savetxt('%ish_%iGeV_%ideg_offaxis_hist_x_fov_old.dat'%(nshower,E_prim,theta_p)
      ,np.transpose([mids,hist])
      ,newline='\n'
      ,fmt="%7.2f %1.6e"
      ,header=(' Num_showers:%i \n E_primary (GeV): %i \n ID_prim_particle: %s \n Seeds: %i, %i \n'
               %(nshower,E_prim,prim_part,seed1,seed2) +
               ' Theta prim. part. incidence: %i deg \n Obs level (m): %i \n Atmosp model: %i'
               %(theta_p,obs_level ,atm_mod) +
               '\n Cerenk_bunch_size: %i \n Fluor_bunch_size: %i'
               %(cersize,fluorsize) +
               '\n  \n Distance to shower axis (m) | Phot_density(1/m2)'
               )
              )
print(lines)