
# coding: utf-8

# In[212]:


from scipy.io import FortranFile
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from math import pi
from itertools import compress
from astropy.io import ascii


# In[327]:


print('#############################  DATA CARD ##############################','\n')
with open('all', 'r') as f:
    read_data = f.read()
    print(read_data)
    datacard=ascii.read(read_data,format='fixed_width_no_header', delimiter=' '
                        ,col_starts=(0, 8, 39)
#                         ,col_ends=(7, 38, 150)
                       )
f.closed
print('#######################################################################','\n')

nshower=int(datacard[2][1].split()[0])
# print('Num_showers:',nshower)
E_prim=float(datacard[5][1].split()[1])
# print('E_primary (GeV):',E_prim)
# prim_part=datacard[3][1]
# if prim_part =="1":
#     print('ID_prim_particle: gamma')
#     prim_part='gamma'
# if prim_part =="14":
#     print('ID_prim_particle: proton')
# seed1=datacard[8][1]
# seed2=datacard[9][1]
# print('Seeds:',seed1,',',seed2)
# theta_p=datacard[6][1]
# print('Theta prim. part. incidence:',theta_p, 'deg')
# phi_p=datacard[7][1]
# print('Phi prim. part. incidence:',phi_p, 'deg')
# obs_level=datacard[10][1]
# obs_level=obs_level*1e-2 #in meters
# print('Obs level (masl):',obs_level)
# atm_mod=datacard[18][1]
# print('Atmosp model:',atm_mod)
# cersize=datacard[23][1]
# print('Cerenk_bunch_size:',cersize)
# fluorsize=datacard[24][1]
# print('Fluor_bunch_size:',fluorsize)
x_area=float(datacard[27][1].split()[4])
y_area=float(datacard[27][1].split()[5])
# print('Detector size:',x_area)
print('---------------------')


# In[329]:


file = FortranFile('CER000001', 'r')
col = ['Nphot','x','y','u','v','arrtime','height']
df = pd.DataFrame(columns=col)

while True:
    try:
        data = np.split(file.read_reals(dtype=np.float32).reshape(819,7),21) #21 subblocks of 39 lines each
        indices = [0<np.abs(i[0][0])<3000 for i in data]
        indices_num = [np.abs(i[0][0]) for i in data]
        bunches = list(compress(data,indices))
        bunches_all=np.vstack(bunches)
#         Store into dataframe
        df=df.append(pd.DataFrame.from_records(bunches_all,columns=col))
#         In case of having several events, they can be distinguished with an changing index each time 
#         a certain condition finds an event end and a new event header. See for example:
# (819, 7)
# (819, 7)
# (741, 7) <---
# (819, 7)
# (819, 7)
          
    except:
#         print('File End')
        break

df = df.loc[(df!=0).any(axis=1)]  #drop rows with zeros
# Define cosine in z direction 'w'
df = df.assign(x=1e-2*df.x) 
df = df.assign(y=1e-2*df.y) 
df = df.assign(w=1-(df.u)**2-(df.v)**2) #-> POSIBLES PROBLEMAS CON REDONDEO


# In[330]:


# Establish some flag indicating which type of histogramming will be used.

if x_area == y_area:
    # Define radial distance to the core of the shower axis.
    df = df.assign(r=np.sqrt((df.x)**2+(df.y)**2))
    df=df[df.r < maxlen]
    print("Histogramming radially")
    binsize = 1 #meters
    maxlen  = 1e-2 * x_area / 2 #meters
    numbins = int (maxlen / binsize)
    radmax=1.e3/2
    breaks=numbins+1
    radius=np.linspace(0,maxlen,breaks)
    mids=((radius[1]-radius[0])/2)+radius
    mids=mids[0:numbin]
    ring=pi*((radius[1:])**2-(radius[0:numbin])**2)
    ring2=ring[(df.r/(radius[1]-radius[0])).astype(int)]
    df = df.assign(weighted_pht=np.abs(df.Nphot)/ring2*nshower)   
    # Store the histogram into a plain text
    # ...
     
    # Ploting
    plt.figure()
    hist=df.r.hist(bins=numbins,log=True, weights=df.weighted_pht ,histtype='step'
#                    ,range=[0.,maxlen]
                  )
    plt.show()

else:
    # Define a column with 'weights' to histogram later: Histogramming along x-axis
    print("Histogramming along x-axis")
    binsize = 1 #meters
    maxlen  = 1e-2 * x_area / 2
    numbins = int(maxlen / binsize)
    df = df.assign(weighted_pht=np.abs(df.Nphot)/binsize**2)
          
    # Store the histogram into a plain text
    # ...
     
    # Ploting
    plt.figure()
    hist=df.x.hist(bins=numbins,log=True, weights=df.weighted_pht ,histtype='step',range=[0.,maxlen])
    plt.show()

