from scipy.io import FortranFile
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from math import pi
from itertools import compress
from astropy.io import ascii
import matplotlib.style
import matplotlib as mpl
import sys

''' 
Ways to histogram showers (analogous to indicate where the telescope
is pointing):
  - on axis if theta_telesc = theta_primary
  - off axis (else)
  
Run this script as:

    python reader.py <filename_from_CORSIKA>

Then sys.argv[1] gets <filename_from_CORSIKA> as the 
file that will be analyze here. 
Also, the "all" DATA CARD file must be in the same directory.

To do:
  - Establish any way of pointing the telescope to any direction
    via theta angle. 
  - If any argument is passed, CER000001 should be set
    as default input file.
  - Distinguish fluor/Cherenkov photons in order to histogramming both 
    components separately.

'''

with_fov = input("Include FoV (y/n)? ")

def histogram(bunches,x_area,y_area,theta,with_fov):
    fov=np.cos(5*pi/180) #FoV contraint (+/- 5 deg)
    
    # Histogramming along x-axis
    if x_area != y_area:  
        weighted_pht=np.abs(bunches[:,0])/(binsize**2*nshower)
        if with_fov=='n': # all photons
            h,edges=np.histogram(1e-2*bunches[:,1]
				,bins=distances
				,weights=weighted_pht
				,range=[0.,maxlen])
        else: # 10 deg FoV
            wemis=np.sqrt(1-bunches[:,3]**2-bunches[:,4]**2)
            # Pointing telescope along the shower direction (on-axis) 
            # if theta(tel)!=theta_p -> off-axis observation
            wemis=wemis*np.cos(-theta*pi/180)+bunches[:,3]*np.sin(-theta*pi/180) 
            # where uemis=bunches[:,3]
            bunches_fov=bunches[(bunches[:,1]>=0)&(wemis>=fov)]
            weighted_pht_fov=weighted_pht[(bunches[:,1]>=0)&(wemis>=fov)]
            h,edges=np.histogram(1e-2*bunches_fov[:,1]
				,bins=distances
				,weights=weighted_pht_fov
				,range=[0.,maxlen])
    
    # Histogramming radially
    else:  
        r=np.sqrt((1e-2*bunches[:,1])**2+(1e-2*bunches[:,2])**2)
        bunches=bunches[r < maxlen]
        r=r[r < maxlen]
        ring2=ring[(r/(radius[1]-radius[0])).astype(int)]
        weighted_pht=np.abs(bunches[:,0])/(ring2*nshower)
        # Store into an histogram
        if with_fov=='n':
            h,edges=np.histogram(r
				 ,bins=radius
				 ,weights=weighted_pht
				 ,range=[0.,maxlen]
				 )
        else:
            wemis=np.sqrt(1-bunches[:,3]**2-bunches[:,4]**2)
            wemis=wemis*np.cos(-theta*pi/180)+bunches[:,3]*np.sin(-theta*pi/180) 
            # where uemis=bunches[:,3]
            bunches_fov = bunches[wemis >= fov]
            r_fov = r[wemis >= fov]
            ring2_fov=ring[(r_fov/(radius[1]-radius[0])).astype(int)]
            weighted_pht_fov=np.abs(bunches_fov[:,0])/(ring2_fov*nshower)
            h,edges=np.histogram(r_fov
				 ,bins=radius
				 ,weights=weighted_pht_fov
				 ,range=[0.,maxlen]
				 )
    return(h)

# =========  DATA CARD =========
with open('all', 'r') as f:
    read_data = f.read()
    #print(read_data) 
    datacard=ascii.read(read_data,format='fixed_width_no_header'
			, delimiter=' '
                        ,col_starts=(0, 8, 39))
f.closed

nshower=int(datacard[2][1].split()[0])
#print('Num_showers:',nshower)
E_prim=float(datacard[5][1].split()[1])
#print('E_primary (GeV):',E_prim)
prim_part=int(datacard[3][1].split()[0])
if prim_part ==1:
    #print('ID_prim_particle: gamma')
    prim_part='gamma'
if prim_part ==14:
    prim_part='proton'
seed1=int(datacard[8][1].split()[0])
seed2=int(datacard[9][1].split()[0])
#print('Seeds:',seed1,',',seed2)
theta_p=float(datacard[6][1].split()[0])
print('Theta prim. part. incidence:',theta_p, 'deg')
phi_p=float(datacard[7][1].split()[0])
#print('Phi prim. part. incidence:',phi_p, 'deg')
obs_level=float(datacard[10][1].split()[0])
obs_level=obs_level*1e-2 #in meters
#print('Obs level (masl):',obs_level)
atm_mod=int(datacard[18][1].split()[0])
#print('Atmosp model:',atm_mod)
cersize=float(datacard[23][1].split()[0])
#print('Cerenk_bunch_size:',cersize)
fluorsize=float(datacard[24][1].split()[0])
#print('Fluor_bunch_size:',fluorsize)
x_area=float(datacard[27][1].split()[4])
y_area=float(datacard[27][1].split()[5])
#print('Detector size:',x_area)
#print('---------------------')

#b = input("How many blocks to store? ")
b = 10 # the most efficient number of them

file = FortranFile(sys.argv[1], 'r')
# Histograms definition
binsize = 10 #meters
maxlen  = 1e-2 * x_area / 2
numbins = int(maxlen / binsize)
breaks=numbins+1
bunches = np.array([]).reshape(0,7)
#with_fov='t'

if x_area != y_area:
    distances=np.linspace(0,maxlen,breaks) # x
    mids=((distances[1]-distances[0])/2)+distances
    mids=mids[0:numbins]
    type_of_hist='x'
    print("Histogramming along x-axis...")
    hist=np.zeros(numbins)
else:
    radius=np.linspace(0,maxlen,breaks) # r
    mids=((radius[1]-radius[0])/2)+radius
    mids=mids[0:numbins]
    ring=pi*((radius[1:])**2-(radius[0:numbins])**2)
    type_of_hist='r'
    print("Histogramming radially...")
    hist=np.zeros(numbins)

count=0
lines=0

while True:
    try:
        count=count+1
        data = np.split(file.read_reals(dtype=np.float32).reshape(819,7),21) 
        #i.e. 21 subblocks of 39 lines each
        indices = [0<np.abs(i[0][0])<3000 for i in data]
        bunches = np.vstack([bunches, np.vstack(list(compress(data,indices)))])
        if count==int(b):
            lines=lines+len(bunches)
            h = histogram(bunches,x_area,y_area,theta_p,with_fov)
            hist  = hist + h
            count = 0 
            bunches = np.array([]).reshape(0,7) # reset array
    except:
        lines=lines+len(bunches)
        h = histogram(bunches,x_area,y_area,theta_p,with_fov)
        hist = hist + h
        break
print(lines)

if with_fov=='n':
    print("...not including FoV constraint")
    np.savetxt('%ish_%iGeV_%ideg_offaxis_hist_%s_all_phots.dat'%(nshower,E_prim,theta_p,type_of_hist)
      ,np.transpose([mids,hist])
      ,newline='\n'
      ,fmt="%7.2f %1.6e"
      ,header=(' Num_showers:%i \n E_primary (GeV): %i \n ID_prim_particle: %s \n Seeds: %i, %i \n'
               %(nshower,E_prim,prim_part,seed1,seed2) +
               ' Theta prim. part. incidence: %i deg \n Obs level (m): %i \n Atmosp model: %i'
               %(theta_p,obs_level ,atm_mod) +
               '\n Cerenk_bunch_size: %i \n Fluor_bunch_size: %i'
               %(cersize,fluorsize) +
               '\n  \n Distance to shower axis (m) | Phot_density(1/m2)'
               )
              )
    print('Histogram stored into: %ish_%iGeV_%ideg_offaxis_hist_%s_all_phots.dat'%(nshower,E_prim,theta_p,type_of_hist))
else:
    print("...including FoV constraint")
    np.savetxt('%ish_%iGeV_%ideg_offaxis_hist_%s_fov.dat'%(nshower,E_prim,theta_p,type_of_hist)
      ,np.transpose([mids,hist])
      ,newline='\n'
      ,fmt="%7.2f %1.6e"
      ,header=(' Num_showers:%i \n E_primary (GeV): %i \n ID_prim_particle: %s \n Seeds: %i, %i \n'
               %(nshower,E_prim,prim_part,seed1,seed2) +
               ' Theta prim. part. incidence: %i deg \n Obs level (m): %i \n Atmosp model: %i'
               %(theta_p,obs_level ,atm_mod) +
               '\n Cerenk_bunch_size: %i \n Fluor_bunch_size: %i'
               %(cersize,fluorsize) +
               '\n  \n Distance to shower axis (m) | Phot_density(1/m2)'
               )
              )  
    print('Histogram stored into: %ish_%iGeV_%ideg_offaxis_hist_%s_fov.dat'%(nshower,E_prim,theta_p,type_of_hist))